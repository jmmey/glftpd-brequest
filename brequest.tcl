set reqbin /glftpd/bin/brequest

#---------------------------------------------------------------------------------------------

bind pub - !requests 	pub:requests
bind pub - !request 	pub:request
bind pub - !reqfilled	pub:reqfilled
bind pub - !reqdel		pub:reqdel
bind pub - !reqwipe		pub:reqwipe

proc strip {in} {
	regsub -all -- {\003[0-9]{0,2}(,[0-9]{0,2})?|\017|\037|\002|\026|\007} $in {} out
	return $out
}


proc pub:requests {nick uhost handle chan arg} {
	global reqbin

	set found 0

	set arg [strip $arg]

	if { $arg != "" } {
		set output [exec $reqbin BOT SHOW_REQUESTS $arg]
	} else {
		set output [exec $reqbin BOT SHOW_REQUESTS]
	}

	if { [lindex $output 0] == "ERROR" } {
		set line [string range $output [expr [string length [lindex $output 0]]+1] end]
		putquick "PRIVMSG $chan :\[\002\0034REQUESTS\017\] Error: $line"
		return
	}

	foreach line [split $output "\n"] {
		if { [lindex $line 0] != "LIST" } { continue }

		set rnum 	[lindex $line 1]
		set req		[lindex $line 2]
		set requester	[lindex $line 3]
		set timestamp	[lindex $line 4]

		putquick "PRIVMSG $chan :\[\002\0037REQUESTS\017\] \[\002$rnum\002\] \002$req\002 - By $requester ([clock format $timestamp -format "%c"])"
		incr found
	}

	if { $found == 0 } {
		putquick "PRIVMSG $chan :\[\002\0034REQUESTS\017\] There are \002no\002 requests at the moment."
	}
}

proc pub:request {nick uhost handle chan arg} {
	global reqbin

	set arg [string map {( \\( ) \\)} [strip $arg]]
	set arg [string map { " " "_" } $arg ]

	if { $arg == "" } {
		putserv "PRIVMSG $chan :\[\002\0034REQUEST\017\] Correct syntax is !request \[RELEASE-NAME\]!"
		return
	}

	set output [exec $reqbin BOT REQUEST $nick $arg]


	foreach line [split $output "\n"] {
		if { [lindex $line 0] == "ERROR" } {
			set  line [string range $line [expr [string length [lindex $line 0]]+1] end]
			putquick "PRIVMSG $chan :\[\002\0034REQUEST\017\] Error: $line"
		}
	}
}

proc pub:reqfilled {nick uhost handle chan arg} {
	global reqbin

	set arg [string map {( \\( ) \\)} [strip $arg]]
	set arg [string map { " " "_" } $arg ]

	if { $arg == "" } {
		putserv "PRIVMSG $chan :\[\002\0034REQFiLLED\017\] Correct syntax is !reqfilled \[RELEASE-NAME\] or !reqfilled \[REQUEST-NUMBER\]!"
		return
	}

	set output [exec $reqbin BOT REQFILLED $nick $arg]

	foreach line [split $output "\n"] {
		if { [lindex $line 0] == "ERROR" } {
			set  line [string range $line [expr [string length [lindex $line 0]]+1] end]
			putquick "PRIVMSG $chan :\[\002\0034REQFiLLED\017\] Error: $line"
		}
	}
}

proc pub:reqdel {nick uhost handle chan arg} {
	global reqbin

	set arg [string map {( \\( ) \\)} [strip $arg]]
	set arg [string map { " " "_" } $arg ]

	if { $arg == "" } {
		putserv "PRIVMSG $chan :\[\002\0034REQDEL\017\] Correct syntax is !reqdel \[RELEASE-NAME\] or !reqdel \[REQUEST-NUMBER\]!"
		return
	}

	set output [exec $reqbin BOT REQDEL $nick $arg]

	foreach line [split $output "\n"] {
		if { [lindex $line 0] == "ERROR" } {
			set  line [string range $line [expr [string length [lindex $line 0]]+1] end]
			putquick "PRIVMSG $chan :\[\002\0034REQDEL\017\] Error: $line"
		}
	}
}

proc pub:reqwipe {nick uhost handle chan arg} {
	global reqbin

	set arg [string map {( \\( ) \\)} [strip $arg]]
	set arg [string map { " " "_" } $arg ]

	if { $arg == "" } {
		putserv "PRIVMSG $chan :\[\002\0034REQWiPE\017\] Correct syntax is !reqwipe \[RELEASE-NAME\] or !reqwipe \[REQUEST-NUMBER\]!"
		return
	}

	if {![isop $nick]} {
		putserv "PRIVMSG $chan :\[\002\0034REQWiPE\017\] Only for ops shorty!"
		return
	}

	set output [exec $reqbin BOT REQWIPE $nick $arg]

	foreach line [split $output "\n"] {
		if { [lindex $line 0] == "ERROR" } {
			set  line [string range $line [expr [string length [lindex $line 0]]+1] end]
			putquick "PRIVMSG $chan :\[\002\0034REQWiPE\017\] Error: $line"
		}
	}
}
putlog "BREQUEST - TEAM B.O.R 2004"


