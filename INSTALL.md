# /* BREQUEST */

## What is it?

This is forked and modified version of the original released `BREQUEST.V1.0.GLFTPD.ADDON-BOR` by `TEAM B.O.R` on `2004-10-11`

A request script with the usual features.

## Features:

- Written in C
- You can limit users to x requests per month
- Tax option, removes x MB from a user if he adds a request
- Autoclean option that wipes old requests
- IRC command !requests can filter for words in request name

## How do I install it??

- Extract the tarball

- edit the `config.h`.
    In case you specified your old request file in `config.h`,
    make sure it is empty.

- Create the config file, ie `touch /glftpd/ftp-data/misc/brequest`.

- Create the tmpdir if it does not exist, ie `mkdir /glftpd/tmp`

- Compile the binary. `gcc brequest.c -o brequest`

- Copy the binary to glftpd's bin dir. `cp brequest /glftpd/bin`

- `chown root /glftpd/bin/brequest`

- `chmod 4755 /glftpd/bin/brequest`

- Add the following lines to your glftpd.conf:
```
    site_cmd        REQUESTS        EXEC    /bin/brequest[:space:]SHOW_REQUESTS
    custom-requests     *
    site_cmd        REQUEST         EXEC    /bin/brequest[:space:]REQUEST
    custom-request      *
    site_cmd        REQFILLED       EXEC    /bin/brequest[:space:]REQFILLED
    custom-reqfilled    *
    site_cmd        REQDEL          EXEC    /bin/brequest[:space:]REQDEL
    custom-reqdel       *
    site_cmd        REQWIPE         EXEC    /bin/brequest[:space:]REQWIPE
    custom-reqwipe      1
```
**NOTE: Change the permissions to your needs.**

- If you want irc support:

    - Edit the `brequest.tcl` and change the path to the binary.

    - Copy `brequest.tcl` to `your_egg_dir/scripts`

    - Add `source scripts/brequest.tcl` to your `eggdrop.conf`

    - Edit your `ngBot.tcl` and add the following lines:
```
        set msgtypes(DEFAULT) "REQUEST REQFILLED REQDEL REQWIPE"

        set chanlist(REQDEL)        "#yourchan"
        set chanlist(REQUEST)       "#yourchan"
        set chanlist(REQFILLED)	    "#yourchan"
        set chanlist(REQWIPE)       "#yourchan"

        set disable(REQUEST)	    0
        set disable(REQFILLED)	    0
        set disable(REQDEL)	        0
        set disable(REQWIPE)        1

        set variables(REQUEST)		"%user %request"
        set variables(REQFILLED)	"%user %request %passed_time"
        set variables(REQDEL)		"%user %request"
        set variables(REQWIPE)		"%user %request"

        set announce(REQUEST)		"\[\002\0037REQUEST\003\002\] \002%user\002 added an request for \002%request\002. Please fill as soon as possible."
        set announce(REQFILLED)		"\[\002\0037REQFiLLED\003\002\] \002%user\002 filled \002%request\002. \002%passed_time\002 passed since the request was added."
        set announce(REQDEL)		"\[\002\0037REQDEL\003\002\] \002%user\002 removed \002%request\002 from the request list."
        set announce(REQWIPE)		"\[\002\0037REQWIPE\003\002\] \002%user\002 wiped \002%request\002."
```
## How do I request, fill a request etc?

- `site request rlsname`                                         <-- To add a request
- `site reqfilled rlsname` or `site reqfilled request number`    <-- To mark a request as filled
- `site reqdel rlsname` or `site reqdel request number`          <-- deletes a request from the request list
- `site reqwipe rlsname` or `site reqwipe request number`        <-- removes the request from the req list and removes the reqdir
- `site requests` or `site requests text`                        <-- shows the request list. If you do site request 5 it will show 5 requests instead of all.

The irc commands are !request, !requests, !reqdel, !reqwipe and !reqdel

## What is the difference between reqwipe and reqdel??

Well, reqdel is supposed to be used if a user accidently adds a request.\
Reqdel will only work if the request dir is empty and you can only\
delete requests that you have added yourself. In case you use the `TAX` or\
`MONTHLIMIT` option the user will get his credits back and/or his\
`MONTHLIMIT` will be decremented.

Reqwipe is for siteops that wants to clean request. If you wipe a request\
it will get removed from the request list as well as the request dir\
will be removed. If you use the `MONTHLIMIT` and/or the `TAX` option the\
user who added the request will NOT get his credits back, neither will\
his monthlimit get decremented.

## I keep reading about a `MONTHLIMIT` and a `TAX` option. The fluck is it???

If you are a nice siteop and allow all users to add request you may\
want to prevent them from abusing this neat gesture. You can do that by\
limiting users to X request per month and you can also specify a amount of\
credits that a user has to "pay" for each request he adds.\
Set the options to 0 if you dont want this feature.

**NOTE**: In case you user either the `MONTHLIMIT` and/or the `TAX` option\
!request and !reqdel are disabled because it is not possible to match\
a irc user against a site user since most people dont use the same nick\
on the site and on irc.


## Is there a way to clean old requests?

Indeed there is. In config.h you see 2 options, `WIPE_FILLED` and\
`WIPE_NONFILLED`. They specify how old a filled or non filled request\
has to be to be wiped.

If you want to use the autoclean feature, add a crontab that runs once a day

`01 00 * * *		/glftpd/bin/brequest CLEAN`



**originally built by TEAM B.O.R 2004**
