// this groups are excluded from TAX and MONTHLIMIT
#define EXLUDEDGRPS             "SITEOPS SITETECHS"

// will be used in ascii footer
#define SITE_SHORT               "BOR"

// will be used in ascii footer
#define CURRENT_YEAR             "2022"

// Absolute path to your glftpd dir
#define GLDIR			        "/glftpd"

// tmpdir, relative to your glftpd dir. Make sure its world wide writable (777)
#define TMPDIR                  "/tmp"

// relative path to your request file.
#define REQFILE			    "/ftp-data/misc/brequests"

// relative path to your request dir.
#define REQDIR			    "/site/REQUESTS"

// prefix for requests.
#define REQPREFIX		        "[REQ]-"

// prefix for filled requests
#define FILLEDPREFIX		    "[FiLLED]-"

/* If you set this to 100 the request script would remove 100 mb from the user who adds a request.
   Set it to 0 if you dont wanna use this feature. If you set it !request and !reqdel
   are disabled because we cant match a irc user against a site user */
#define TAX                     0

/* You can limit users to x requests/month. Set it to 0 if you dont wanna use this feature.
   If you set it !request and !reqdel are disabled because we cant match a irc user against a site user */
#define MONTHLIMIT              0

// If you use the autoclean feature this specifies how many days a filled request is supposed to stay on the site.
#define WIPE_FILLED             14  // wipe filled reqs after 14 days

// If you use the autoclean feature this specifies how long a non filled request is supposed to stay on the site.
#define WIPE_NONFILLED          30  // wipe non filled reqs after 30 days

// leave that alone for now
#define MAX 255
