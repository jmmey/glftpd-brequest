# v1.1 - 2022-05-21
Imported the original release unchanged and applied following changes
* removed feature to list only limited number of requests on irc example: !requests 5 - to list only 5 requests
* added feature to filter request list example: !requests x265 - to list all requests containing x265
* fixed a case where the full local filesystem paths would be printed to irc/output

# v1.0 - 2004-10-11

* released as `BREQUEST.V1.0.GLFTPD.ADDON-BOR`
