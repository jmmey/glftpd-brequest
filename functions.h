#include "config.h"

struct reqlog {
    time_t reqtime;
    time_t filltime;
    char requester[255];
    char filler[255];
    char rlsname[MAX];
    struct reqlog *nxt;
    struct reqlog *prv;
};

int isexcluded(){
    if(strcmp(getenv("GROUP"), "") == 0)
        return 0;
    if(strstr(EXLUDEDGRPS, getenv("GROUP")) != NULL)
        return 1;
    return 0;
}

int isnumber(char *str){
    int i=0;
    for(i; i < strlen(str); ++i){
        if(str[i] < 48 || str[i] > 57)
            return 1;
    }
    return 0;
}

void print_error(char *error){
    printf("                                       ���  ���� ���� ���� ����                                    \n");
    printf("������������������������������������� ��� ܰ��� ���� ��  ����� ������������������������������������\n");
    printf("�                                       ���   �    �   ��    �                                    �\n");
    printf("�                                                                                                 �\n");
    printf("� %-95.95s �\n", error);
    printf("�                                                                                                 �\n");
    printf("������������������������������������������������������������������������������������� %s %s ����\n", SITE_SHORT, CURRENT_YEAR);
}

char *replacespaces(char *argv[], int argc, char *rls, int start){
    for(start; start < argc; start++){
        strcpy(rls, strcat(rls, argv[start]));
        if(start < argc-1){
            strcpy(rls, strcat(rls,"_"));
        }
    }
    return rls;
}

int takecredits(int givetake) {
    char line[1024];
    char rfile[MAX];
    char tmp[MAX];
    int credits=0;
    FILE *log;
    FILE *tuser;

    if(isexcluded() == 1)
        return 0;


    sprintf(rfile, "/ftp-data/users/%s", getenv("USER"));
    sprintf(tmp, "%s/user.tmp", TMPDIR);

    log=fopen(rfile,"r+");

    if( log == NULL ){
        sprintf(rfile, "Could't open the userfile for %s", getenv("USER"));
        print_error(rfile);
        exit (0);
    }

    tuser=fopen(tmp, "w");

    if( tmp == NULL ){
        print_error("Couldn't create temp file, go bug siteops!");
        exit (0);
    }

    while(! feof(log)){
        fgets(line, sizeof(line), log);

        if(strstr(line, "CREDITS") != NULL){
            sscanf(line, "CREDITS %d", &credits);

            // not enough credits to add requests?
            if( givetake == -1 && credits < TAX * 1024){
                fclose(log);
                sprintf(rfile, "You don't have enough credits to request. You need at least %d MB!", TAX);
                print_error(rfile);
                exit (0);
            }

            fprintf(tuser, "CREDITS %d\n", credits + (1024 * TAX * givetake));
        }
        else{
            fprintf(tuser, line);
        }
    }
    fclose(log);
    fclose(tuser);

    if(rename(tmp, rfile) != 0)
        print_error("Critical error, could'nt rename the tempfile, go bug siteops immediatly!!");

    return 0;
}
int wipe (const char *path){
    DIR *dirp;
    struct dirent *dp;
    struct stat	statinfo;
    char f[MAX];

    if(lstat(path,&statinfo) == -1){
        //perror("lstat");
        return 1;
    }

    switch(statinfo.st_mode & S_IFMT){
        case S_IFDIR :
            // dir
            if ((dirp = opendir(path)) == NULL) {
                //printf("Couldn't open %s\n", path);
                return 1;
            }
            while ((dp = readdir(dirp)) != NULL){
                if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
                    continue;
                sprintf(f, "%s/%s", path, dp->d_name);
                wipe(f);
            }
            if(rmdir(path) != 0)
                return 1;
            break;
        case S_IFLNK :
            // symlink
            if(unlink(path)!= 0)
                return 1;
            break;
        case S_IFREG :
            // file
            if(unlink(path)!= 0)
                return 1;
            break;
    }
    return 0;
}

char *passed_time(time_t timestamp, char *result){
    time_t t = time(NULL);
    int years=0;
    int weeks=0;
    int days=0;
    int hours=0;
    int minutes=0;

    t = time(NULL) - timestamp;

    years = t / 31536000;
    t -= years * 31536000;
    weeks = t / 604800;
    t -= weeks * 604800;
    days = t / 86400;
    t -= days * 86400;
    hours = t / 3600;
    t -= hours * 3600;
    minutes = t / 60;
    t -= minutes * 60;

    if(years != 0)
        sprintf(result, "%d year(s), %d week(s), %d day(s), %d hour(s), %d minute(s), %ld second(s)", years, weeks, days, hours, minutes, t);
    else if (weeks != 0)
        sprintf(result, "%d week(s), %d day(s), %d hour(s), %d minute(s), %ld second(s)", weeks, days, hours, minutes, t);
    else if (days != 0)
        sprintf(result, "%d day(s), %d hour(s), %d minute(s), %ld second(s)", days, hours, minutes, t);
    else if (hours != 0)
        sprintf(result, "%d hour(s), %d minute(s), %ld second(s)", hours, minutes, t);
    else if (minutes != 0)
        sprintf(result, "%d minute(s), %ld second(s)",minutes, t);
    else
        sprintf(result, "%ld second(s)", t);

    return result;
}
int clean_requests(){
    FILE *log;
    struct reqlog rlog;
    char tmp[MAX];
    char rfile[MAX];
    char rdir[MAX];
    time_t t = time(NULL);

    sprintf(rfile,"%s%s", GLDIR, REQFILE);

    // open logfile
    log=fopen(rfile,"rb+");
    if( log == NULL ){
        printf("ERROR Could't open the requestfile\n");
        exit (0);
    }

    while(!feof(log)) {
        if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1 )
            break;

        if(rlog.filltime > 0 && (t - rlog.filltime) >= (WIPE_FILLED * 86400)){
            rlog.filltime = -1;
            fseek(log, -sizeof(struct reqlog), SEEK_CUR);

            if(fwrite(&rlog, sizeof(rlog), 1, log) != 1){
                printf("Couldn't write to the request file.\n");
                fclose(log);
                exit (1);
            }
            sprintf(tmp, "%s%s/%s%s", GLDIR, REQDIR, FILLEDPREFIX, rlog.rlsname);
            if(!wipe(tmp))
                printf("Wiped %s\n", tmp);
            else
                printf("Couldn't wipe %s\n", tmp);
            continue;
        }

        if(rlog.reqtime  > 0 && rlog.filltime == 0 && (t - rlog.reqtime) >= (WIPE_NONFILLED * 86400)){
            rlog.filltime = -1;
            fseek(log, -sizeof(struct reqlog), SEEK_CUR);
            if(fwrite(&rlog, sizeof(rlog), 1, log) != 1){
                printf("Couldn't write to the request file.\n");
                fclose(log);
                exit (1);
            }
            sprintf(tmp, "%s%s/%s%s", GLDIR, REQDIR, REQPREFIX, rlog.rlsname);
            if(!wipe(tmp))
                printf("Wiped %s\n", tmp);
            else
                printf("Couldn't wipe %s\n", tmp);
            continue;
        }
    }
    return 0;
}
int addgllog(int from, char *what){
    FILE *log;
    char tmp[MAX];
    time_t t = time(NULL);
    char s[30];

    strftime(s, 29, "%c", localtime(&t));

    if(from)
        sprintf(tmp,"%s/ftp-data/logs/glftpd.log", GLDIR);
    else
        sprintf(tmp,"/ftp-data/logs/glftpd.log");

    // open logfile
    log=fopen(tmp,"a+");
    if( log == NULL ){
        if(!from)
            print_error("Could't open the glftpd.log, go bug siteops!\n");
        else
            printf("ERROR Could't open the glftpd.log, go bug siteops!\n");
        fclose(log);
        return 0 ;
    }

    fprintf(log, "%s %s\n", s, what);

    fclose(log);

    return 0;

}
