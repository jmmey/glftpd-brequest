#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include "functions.h"

#define VERSION 1.2

int addrequest(int frombot, char *who, char *rls)
{
    int found = -1;
    int i = 0;
    int limit = 0;
    char rdir[MAX];
    char rfile[MAX];
    char tmp[10];
    char tmp2[10];
    time_t t = time(NULL);
    struct reqlog rlog;
    FILE *log;

    if (frombot && (MONTHLIMIT || TAX))
    {
        printf("ERROR This command is disabled because you using either the TAX or MONTHLIMIT option.\n");
        exit(0);
    }

    if (frombot)
        sprintf(rdir, "%s%s/%s%s", GLDIR, REQDIR, REQPREFIX, rls);
    else
        sprintf(rdir, "%s/%s%s", REQDIR, REQPREFIX, rls);

    if (frombot)
        sprintf(rfile, "%s%s", GLDIR, REQFILE);
    else
        sprintf(rfile, "%s", REQFILE);

    // open logfile
    log = fopen(rfile, "rb+");

    if (log == NULL)
    {
        if (frombot)
            printf("ERROR Could't open the requestfile, go bug siteops!\n");
        else
            print_error("Could't open the requestfile, go bug siteops!");
        exit(0);
    }

    // check for duperequest
    while (!feof(log))
    {
        if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
            break;

        // request already exists
        if (strcmp(rlog.rlsname, rls) == 0 && rlog.reqtime > 0 && rlog.filltime == 0)
        {
            sprintf(rdir, "\002%s\002 already added a request for \002%s\002!", rlog.requester, rls);
            if (frombot)
                printf("ERROR %s\n", rdir);
            else
                print_error(rdir);
            fclose(log);
            exit(0);
        }

        // check for deleted/wiped requests to reuse the space in the reqfile
        strftime(tmp, 10, "%m%y", localtime(&rlog.reqtime));
        strftime(tmp2, 10, "%m%y", localtime(&t));

        if ((rlog.filltime != 0 && found == -1 && strcmp(tmp, tmp2) != 0) || (rlog.filltime == 0 && rlog.reqtime == 0 && found == -1))
            found = i;

        // increase limit counter.
        if (strcmp(rlog.requester, who) == 0 && strcmp(tmp, tmp2) == 0)
            ++limit;
        ++i;
    }

    if (!frombot)
    {
        if (limit >= MONTHLIMIT && !isexcluded() && MONTHLIMIT > 0)
        {
            sprintf(tmp, "Your monthly limit of %d request(s) is already reached.", MONTHLIMIT);
            print_error(tmp);
            exit(0);
        }
    }

    // daten ins struct packen
    rlog.reqtime = t;
    rlog.filltime = 0;
    strcpy(rlog.requester, who);
    strcpy(rlog.rlsname, rls);

    // we found a wiped or deleted request in the file and can reuse the space
    if (found != -1)
        fseek(log, -(sizeof(struct reqlog) * (i - found)), SEEK_CUR);

    // take the credits
    if (TAX && !frombot)
        takecredits(-1);

    // write config
    if (fwrite(&rlog, sizeof(struct reqlog), 1, log) != 1)
    {
        if (frombot)
            printf("Could not add your request to the request file! Go bug siteops.\n");
        else
            print_error("ERROR Could not add your request to the request file! Go bug siteops.");
        fclose(log);
        exit(0);
    }

    fclose(log);

    // int mkdir(const char *pathname, mode_t mode);
    umask(000);

    if (mkdir(rdir, 0777) != 0)
    {
        if (frombot)
            printf("ERROR Could't create the request dir. Go bug siteops.\n");
        else
            print_error("Could't create the request dir. Go bug siteops.");
        exit(0);
    }

    sprintf(rdir, "REQUEST: \"%s\" \"%s\"", who, rlog.rlsname);
    addgllog(frombot, rdir);

    if (!frombot)
    {
        printf("                                 ���� ���  ���� �  � ���  ��   ��                                  \n");
        printf("�������������������������������� ��� ��� ܰ�  ���  ۰�� ܰ��߲��  � �������������������������������\n");
        printf("�                                  �   ���  ���  ���  ���  ��   ���                               �\n");
        printf("�                                                                                                 �\n");
        printf("� Added to the request list:                                                                      �\n");
        printf("�                                                                                                 �\n");
        printf("� %-95.95s �\n", rls);
        printf("�                                                                                                 �\n");
        if (MONTHLIMIT)
        {
            sprintf(tmp, "%d", (MONTHLIMIT - limit) - 1);
            printf("� %78.78s request(s) left. �\n", tmp);
            printf("�                                                                                                 �\n");
        }
        printf("������������������������������������������������������������������������������������� %s %s ����\n", SITE_SHORT, CURRENT_YEAR);
    }

    return 0;
}

int reqfilled(int frombot, char *who, char *what)
{
    struct reqlog rlog;
    FILE *log;
    time_t t = time(NULL);
    char dir[MAX];
    char rls[MAX];
    char tmp[MAX];
    char tmp2[MAX];
    char rfile[MAX];
    int i = 1;

    if (frombot)
        sprintf(rfile, "%s%s", GLDIR, REQFILE);
    else
        sprintf(rfile, "%s", REQFILE);

    // open logfile
    log = fopen(rfile, "rb+");

    if (log == NULL)
    {
        if (!frombot)
            print_error("Could't open the requestfile.");
        else
            printf("ERROR Could't open the requestfile.\n");
        exit(0);
    }

    if (isnumber(what) == 1)
    {
        while (!feof(log))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
                break;
            if (strcmp(rlog.rlsname, what) == 0 && rlog.filltime == 0)
            {
                i = 0;
                break;
            }
        }
    }
    else
    {
        while (!feof(log) && i <= atoi(what))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
                break;
            if (i == atoi(what) && rlog.filltime == 0)
            {
                i = 0;
                break;
            }
            ++i;
        }
    }

    if (i != 0)
    {
        if (!frombot)
            print_error("No such request or request number!");
        else
            printf("ERROR \002No\002 such request or request number!\n");
        fclose(log);
        exit(0);
    }
    else if (rlog.filltime != 0)
    {
        sprintf(dir, "%s has already been filled by %s!", rlog.rlsname, rlog.filler);
        if (!frombot)
            print_error(dir);
        else
            printf("ERROR \002%s\002 has already been filled by \002%s\002!\n", rlog.rlsname, rlog.filler);
        exit(0);
    }

    strcpy(rlog.filler, who);
    rlog.filltime = t;
    strcpy(rls, rlog.rlsname);
    t = rlog.reqtime;

    // pointer zurueckschieben
    fseek(log, -sizeof(struct reqlog), SEEK_CUR);

    // update logfile
    if (fwrite(&rlog, sizeof(rlog), 1, log) != 1)
    {
        if (!frombot)
            print_error("Couldn't write to the request file!");
        else
            printf("ERROR Couldn't write to the request file!\n");
        fclose(log);
        exit(0);
    }
    fclose(log);

    if (!frombot)
    {
        sprintf(dir, "%s/%s%s", REQDIR, REQPREFIX, rls);
        sprintf(tmp, "%s/%s%s", REQDIR, FILLEDPREFIX, rls);
    }
    else
    {
        sprintf(dir, "%s%s/%s%s", GLDIR, REQDIR, REQPREFIX, rls);
        sprintf(tmp, "%s%s/%s%s", GLDIR, REQDIR, FILLEDPREFIX, rls);
    }

    if (rename(dir, tmp) != 0)
    {
        sprintf(rls, "Couldn't rename %s to %s.", dir, tmp);
        if (!frombot)
        {
            print_error(rls);
            fprintf(stderr, "ERROR Couldn't rename %s to %s.\n", dir, tmp);
        }
        else
        {
            printf("ERROR Couldn't rename dir of %s\n", rls);
            fprintf(stderr, "ERROR Couldn't rename %s to %s.\n", dir, tmp);
        }
        exit(0);
    }

    passed_time(t, tmp);

    sprintf(dir, "REQFILLED: \"%s\" \"%s\" \"%s\"", who, rlog.rlsname, tmp);
    addgllog(frombot, dir);

    if (!frombot)
    {
        printf("                            ���� ���  ���� ����  � �    �    ���  ���                              \n");
        printf("��������������������������� ��� ��� ܰ�  � �� ��߲��  ܰ�  ܰ�� ܰ�  � ����������������������������\n");
        printf("�                             �   ���  ��� �     �  ���  ���  ��� ���                             �\n");
        printf("�                                                                                                 �\n");
        printf("� Thanks for filling                                                                              �\n");
        printf("�                                                                                                 �\n");
        printf("� %-95.95s �\n", rls);
        printf("�                                                                                                 �\n");
        printf("� %59.59s passed since the request was added. �\n", tmp);
        printf("�                                                                                                 �\n");
        printf("������������������������������������������������������������������������������������� %s %s ����\n", SITE_SHORT, CURRENT_YEAR);
    }
    return 0;
}
int showrequests(int frombot, char *filter)
{
    int i = 0;
    struct reqlog rlog;
    char t[15] = "0";
    char dir[MAX];
    FILE *log;
    if (filter != NULL)
    {
        printf("%s\n", filter);
    }
    if (frombot == 1)
    {
        sprintf(dir, "%s%s", GLDIR, REQFILE);

        // open logfile
        log = fopen(dir, "rb+");
        if (log == NULL)
        {
            printf("ERROR Could not open the requestfile\n");
            exit(0);
        }

        while (!feof(log))
        {
            ++i;
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
            {
                break;
            }
            if (rlog.filltime == 0 && rlog.reqtime != 0)
            {
                if (filter != NULL && strcasestr(rlog.rlsname, filter) == NULL)
                {
                    continue;
                }

                printf("LIST %d %s %s %d\n", i, rlog.rlsname, rlog.requester, rlog.reqtime);
            }
        }
        fclose(log);
    }
    else
    {
        ++i;
        log = fopen(REQFILE, "rb+");

        if (log == NULL)
        {
            print_error("Could't open the requestfile. Go bug siteops.");
            exit(0);
        }

        printf("                               ���� ���  ���� �  � ���  ��   ��   ��\n");
        printf("������������������������������ ��� ��� ܰ�  ���  ۰�� ܰ��߲��  ܰ��߲ ����������������������������\n");
        printf("�                                �   ���  ���  ���  ���  ��   ���  ��                             �\n");
        printf("�                                                                                                 �\n");
        printf("� [REQ-NR] [REQUEST-NAME]                                             [REQUESTER]      [TiME]     �\n");
        printf("�                                                                                                 �\n");

        while (!feof(log))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
            {
                break;
            }
            // printf("%d %d\n", rlog.reqtime, rlog.filltime);
            if (rlog.filltime == 0 && rlog.reqtime > 0)
            {
                strftime(t, 15, "%d.%m.%y %H:%M", localtime(&rlog.reqtime));
                sprintf(dir, "%d", i);
                printf("� [%6s]  %-57.57s  %-10.10s  %-14.14s �\n", dir, rlog.rlsname, rlog.requester, t);
            }
            ++i;
        }

        if (strcmp(t, "0") == 0)
        {
            printf("� There are no requests at the moment.                                                            �\n");
        }
        printf("�                                                                                                 �\n");
        printf("������������������������������������������������������������������������������������� %s %s ����\n", SITE_SHORT, CURRENT_YEAR);
        fclose(log);
    }
}

int reqdel(int frombot, char *who, char *rls)
{
    FILE *log;
    char dir[MAX];
    char rfile[MAX];
    int i = 1;
    int found = 0;
    struct reqlog rlog;

    if (frombot && (MONTHLIMIT || TAX))
    {
        printf("ERROR This command is disabled because you using either the TAX or MONTHLIMIT option.\n");
        exit(0);
    }

    if (frombot)
    {
        sprintf(rfile, "%s%s", GLDIR, REQFILE);
    }
    else
    {
        sprintf(rfile, "%s", REQFILE);
    }

    // open logfile
    log = fopen(rfile, "rb+");

    if (log == NULL)
    {
        if (!frombot)
        {
            print_error("Could't open the requestfile.");
        }
        else
        {
            printf("ERROR Could't open the requestfile.\n");
        }
        exit(0);
    }

    if (isnumber(rls) == 0)
    {
        while (!feof(log))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
            {
                break;
            }

            if (i == atoi(rls) && rlog.filltime == 0 && strcmp(rlog.requester, who) == 0)
            {
                rlog.reqtime = 0;
                rlog.filltime = 0;
                found = 1;
                break;
            }
            ++i;
        }
    }
    else
    {
        while (!feof(log))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
            {
                break;
            }
            if (strcmp(rls, rlog.rlsname) == 0 && rlog.filltime == 0 && strcmp(rlog.requester, who) == 0)
            {
                rlog.reqtime = 0;
                rlog.filltime = 0;
                found = 1;
                break;
            }
        }
    }

    if (found == 0)
    {
        if (!frombot)
        {
            print_error("Request name or number does not exist or you are trying to delete someone else's request.");
        }
        else
        {
            printf("ERROR Request name or number does \002not\002 exist or you are trying to delete someone else's request.\n");
        }
        fclose(log);
        exit(0);
    }

    if (!frombot)
    {
        sprintf(dir, "%s/%s%s", REQDIR, REQPREFIX, rlog.rlsname);
    }
    else
    {
        sprintf(dir, "%s%s/%s%s", GLDIR, REQDIR, REQPREFIX, rlog.rlsname);
    }

    if (rmdir(dir) != 0)
    {
        if (!frombot)
        {
            print_error("Couldn't remove the request dir, either it doesn't exist or its not empty.");
        }
        else
        {
            printf("ERROR Couldn't remove the request dir, either it doesn't exist or its not empty.\n");
        }
        exit(0);
    }

    fseek(log, -sizeof(struct reqlog), SEEK_CUR);

    if (TAX && !frombot)
        takecredits(1);

    // update logfile
    if (fwrite(&rlog, sizeof(rlog), 1, log) != 1)
    {
        if (!frombot)
        {
            print_error("Couldn't write to the request file, go bug siteops!");
        }
        else
        {
            printf("Couldn't write to the request file, go bug siteops!\n");
        }
        fclose(log);
        exit(0);
    }
    fclose(log);

    sprintf(dir, "REQDEL: \"%s\" \"%s\"", who, rlog.rlsname);
    addgllog(frombot, dir);

    if (!frombot)
    {
        printf("                                     ���� ���  ���� ���  ���  �\n");
        printf("������������������������������������ ��� ��� ܰ�  ���  ���� ܰ�  � ��������������������������������\n");
        printf("�                                      �   ���  ��� ���   ���  ���                                �\n");
        printf("�                                                                                                 �\n");
        printf("� Removed                                                                                         �\n");
        printf("�                                                                                                 �\n");
        printf("� %-95.95s �\n", rlog.rlsname);
        printf("�                                                                                                 �\n");
        printf("� from the request list.                                                                          �\n");
        printf("�                                                                                                 �\n");
        printf("������������������������������������������������������������������������������������� %s %s ����\n", SITE_SHORT, CURRENT_YEAR);
    }

    return 0;
}

int reqwipe(int frombot, char *who, char *rls)
{
    FILE *log;
    char dir[MAX];
    char logentry[MAX];
    char rfile[MAX];
    int i = 1;
    int found = 0;
    int fill = 0;
    struct reqlog rlog;

    if (frombot)
    {
        sprintf(rfile, "%s%s", GLDIR, REQFILE);
    }
    else
    {
        sprintf(rfile, "%s", REQFILE);
    }

    // open logfile
    log = fopen(rfile, "rb+");

    if (!isnumber(rls))
    {

        while (!feof(log))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
                break;

            if (i == atoi(rls) && rlog.reqtime > 0 && rlog.filltime != -1)
            {
                i = rlog.filltime;
                rlog.filltime = -1;
                found = 1;
                break;
            }
            ++i;
        }
    }
    else
    {
        while (!feof(log))
        {
            if (fread(&rlog, sizeof(struct reqlog), 1, log) < 1)
                break;
            if (strcmp(rls, rlog.rlsname) == 0 && rlog.filltime != -1)
            {
                i = rlog.filltime;
                rlog.filltime = -1;
                found = 1;
                break;
            }
        }
    }

    if (found == 0)
    {
        if (!frombot)
            print_error("Release name or number does not exist.");
        else
            printf("ERROR Release name or number does not exist.\n");
        fclose(log);
        exit(0);
    }

    // pointer zurueckschieben
    fseek(log, -sizeof(struct reqlog), SEEK_CUR);

    // build path to folder
    if (!frombot)
    {
        if (i < 1)
            sprintf(dir, "%s/%s%s", REQDIR, REQPREFIX, rlog.rlsname);
        else
            sprintf(dir, "%s/%s%s", REQDIR, FILLEDPREFIX, rlog.rlsname);
    }
    else
    {
        if (i < 1)
            sprintf(dir, "%s%s/%s%s", GLDIR, REQDIR, REQPREFIX, rlog.rlsname);
        else
            sprintf(dir, "%s%s/%s%s", GLDIR, REQDIR, FILLEDPREFIX, rlog.rlsname);
    }

    // wipe folder
    if (wipe(dir) != 0)
    {
        if (!frombot)
        {
            print_error("Something went wrong deleting the request, go bug siteops!");
        }
        else
        {
            printf("Something went wrong deleting the request, go bug siteops!\n");
        }
        fclose(log);
        exit(0);
    }

    // update logfile
    if (fwrite(&rlog, sizeof(rlog), 1, log) != 1)
    {
        if (!frombot)
        {
            print_error("Could't write to the requestfile, go bug siteops!");
        }
        else
        {
            printf("Could't write to the requestfile, go bug siteops!\n");
        }
        fclose(log);
        exit(0);
    }
    fclose(log);

    // construct gllog entry
    sprintf(logentry, "REQWIPE: \"%s\" \"%s\"", who, rlog.rlsname);
    addgllog(frombot, logentry);

    if (!frombot)
    {
        printf("                                  ���� ���  ���� �   �  � ���� ���\n");
        printf("��������������������������������� ��� ��� ܰ�  ��� � ۰߲���� ��� � �������������������������������\n");
        printf("�                                   �   ���  ���  � �   � �     ���                               �\n");
        printf("�                                                                                                 �\n");
        printf("� Wiped %-90.90s�\n", rlog.rlsname);
        printf("�                                                                                                 �\n");
        printf("������������������������������������������������������������������������������������� %s %s ����\n", SITE_SHORT, CURRENT_YEAR);
    }

    return 0;
}

int main(int argc, char *argv[])
{
    char *prls;
    char rls[MAX] = "";

    if (argc == 1)
    {
        return 0;
    }

    if (strcmp(argv[1], "CLEAN") == 0)
        return clean_requests();

    if (strcmp(argv[1], "BOT") == 0 && argc > 2)
    {

        if (strcmp(argv[2], "REQUEST") == 0 && argc > 4)
            addrequest(1, argv[3], argv[4]);

        else if (strcmp(argv[2], "SHOW_REQUESTS") == 0)
        {
            if (argc > 3)
                showrequests(1, argv[3]);
            else
                showrequests(1, NULL);
        }

        else if (strcmp(argv[2], "REQFILLED") == 0 && argc > 4)
            reqfilled(1, argv[3], argv[4]);

        else if (strcmp(argv[2], "REQDEL") == 0 && argc > 4)
            reqdel(1, argv[3], argv[4]);

        else if (strcmp(argv[2], "REQWIPE") == 0 && argc > 4)
            reqwipe(1, argv[3], argv[4]);
    }
    else
    {
        prls = replacespaces(argv, argc, rls, 2);

        if (strcmp(argv[1], "REQUEST") == 0)
            if (argc < 3)
                print_error("Correct syntax is site request [release-name]!");
            else
                addrequest(0, getenv("USER"), prls);

        else if (strcmp(argv[1], "SHOW_REQUESTS") == 0)
        {
            if (argc > 2 && !isnumber(argv[2]))
                showrequests(0, argv[2]);
            else
                showrequests(0, NULL);
        }

        else if (strcmp(argv[1], "REQFILLED") == 0)
        {
            if (argc < 3)
                print_error("Correct syntax is site reqfilled [release-name] or site reqfilled [release-number]!");
            else
                reqfilled(0, getenv("USER"), prls);
        }

        else if (strcmp(argv[1], "REQDEL") == 0)
        {
            if (argc < 3)
                print_error("Correct syntax is site reqdel [release-name] or site reqdel [release-number]!");
            else
                reqdel(0, getenv("USER"), prls);
        }

        else if (strcmp(argv[1], "REQWIPE") == 0)
        {
            if (argc < 3)
                print_error("Correct syntax is site reqwipe [release-name] or site reqwipe [release-number]!");
            else
                reqwipe(0, getenv("USER"), prls);
        }
    }

    return 0;
}
